require 'test_helper'

class PostTest < ActiveSupport::TestCase

  # test case to find out entered title is empty or not
  test "should be invalid without a title" do
    posts(:one).title =nil
    assert_nil posts(:one).title
    assert_equal false,posts(:one).valid?
  end

  # test case to find out body of post is empty or not
  test "should be invalid without a body" do
    posts(:one).body =nil
    assert_nil posts(:one).body
    assert_equal false,posts(:one).valid?
  end

  # test case to check the type of title entered in post
  test "post title should be string" do
    assert_equal true, posts(:one).title.is_a?(String)
  end

  # test case to check the type of body entered in post
  test "post body should be string" do
    assert_equal true, posts(:one).body.is_a?(String)
  end
end
