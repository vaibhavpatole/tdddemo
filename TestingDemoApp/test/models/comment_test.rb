require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  test "should be invalid without a name" do
    comments(:one).name =nil
    assert_nil comments(:one).name
    assert_equal false,comments(:one).valid?
  end

  # test case to find out comment of Comment is empty or not
  test "should be invalid without a comment" do
    comments(:one).comment =nil
    assert_nil comments(:one).comment
    assert_equal false,comments(:one).valid?
  end

  # test case to check the type of name entered in Comment
  test "Comment name should be string" do
    assert_equal true, comments(:one).name.is_a?(String)
  end

  # test case to check the type of comment entered in Comment
  test "Comment comment should be string" do
    assert_equal true, comments(:one).comment.is_a?(String)
  end
end
