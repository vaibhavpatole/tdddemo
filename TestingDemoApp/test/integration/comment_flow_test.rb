require 'test_helper'

class CommentFlowTest < ActionDispatch::IntegrationTest

  test "can create new comment" do
    # get route for new create post
    get "/posts/1/comments/new"
    assert_response :success
    # creating a sample post for checking
    comment "/comments",
      params: {comment: {name: "Post title",comment:"Post body"}}
    assert_response :redirect
    follow_redirect!
    assert_response :success

    assert_select "h2","Post title"
  end
end
