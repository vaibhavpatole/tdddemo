require 'test_helper'

class PostFlowTest < ActionDispatch::IntegrationTest

  # test to check submited post display on home
  test "can see the post#index on homepage" do
    # to find the root routes
    get "/"
    # to select h1 tag from index page
    assert_select "h1","Post#index"
  end

  #test to check workinng of link from index to new post
  test "can create new post" do
    # get route for new create post
    get "/posts/new"
    assert_response :success
    # creating a sample post for checking
    post "/posts",
      params: {post: {title: "Post title",body:"Post body"}}
    assert_response :redirect
    follow_redirect!
    assert_response :success

    assert_select "h2","Post title"
  end

  # test to check form is created or not
  test "can see form on post#new route" do
    get "/posts/new"

    # for reading element from form
    assert_select "form" do |elements|
      # for reading child element from form
      elements.each do |element|
        assert_select element, "input",3
      end
    end
  end
end
