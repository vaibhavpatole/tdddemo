require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  # test case for getting the index url from routes
  test "should get index" do
    get posts_url
    assert_response :success
  end

  # test case to find the link from routes
  test "should get new" do
    get "/posts/new"
    assert_response :success
  end

  #test to redirect page show post
  test "should get show" do
    get posts_url
    assert_response :success
  end
end
