require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  context 'GET #index' do
    it 'return a success message' do
      get :index
      expect(response.successful?)
    end
  end

  context '#show' do
    it 'return a success message' do
      user = User.new(first_name:'first',last_name:'last', email:'abc@gmail.c0m')
      get:show, params:{id:'1'}
      expect(response.successful?)
    end
  end
end
